#![deny(warnings)]
#![deny(missing_docs)]
#![deny(missing_debug_implementations)]

//! # hyperdav
//! The `hyperdav` crate provides an API for interacting with the WebDAV protocol.
//!
//! It's easy to use and handles all the abstractions over HTTP for the user. Supports async client (default) and
//! blocking client (via cargo feature flag)
//!
//!
//! ## GET request
//!
//! ```rust
//! # extern crate failure;
//! # extern crate hyperdav;
//! # use hyperdav::{AsyncClient};
//! # use failure::Error;
//! #
//! # async fn run() -> Result<(), Error> {
//! let client = AsyncClient::new()
//!     .credentials("foo", "bar")
//!     .build("https://demo.owncloud.org/remote.php/webdav/")
//!     .unwrap();
//!
//! let mut res = client.get(&["file.txt"]).await?;
//! res.bytes().await?.as_ref().to_vec();
//! # Ok(())
//! # }
//! ```
//!
//! The GET request will return a [`Response`][response] from the [`reqwest`][reqwest] crate on
//! success.
//!
//! ## PUT request
//!
//! ```rust
//! # extern crate failure;
//! # extern crate hyperdav;
//! # use hyperdav::{AsyncClient};
//! # use failure::Error;
//! #
//! # async fn run() -> Result<(), Error> {
//! let client = AsyncClient::new()
//!     .credentials("foo", "bar")
//!     .build("https://demo.owncloud.org/remote.php/webdav/")
//!     .unwrap();
//! client.put("", &["file.txt"]).await?;
//!     # Ok(())
//! # }
//!
//! ```
//!
//! The PUT request will return `()` on success just to indicate it succeeded
//!
//! ## Blocking client
//!
//! For blocking client enable the blocking feature and check the blocking client docs
//!
//! ```toml
//! hyperdav = { version = "0.2", features = ["blocking", "default-tls"] }
//! ```
//!
//! [response]: ./struct.Response.html
//! [reqwest]: https://crates.io/crates/reqwest

use std::string::ToString;

pub use reqwest::Response;

#[cfg(feature = "blocking")]
pub use crate::client::{Client, ClientBuilder};

pub use crate::async_client::{AsyncClient, AsyncClientBuilder};

pub use crate::error::Error;
pub use crate::response::PropfindResponse;

#[cfg(feature = "blocking")]
mod client;

mod async_client;
mod error;
mod response;

#[derive(Debug)]
/// Define the depth to which we should search.
pub enum Depth {
    /// Any depth you want
    Number(u32),
    /// As deep as we can go
    Infinity,
}

impl ToString for Depth {
    fn to_string(&self) -> String {
        match *self {
            Depth::Number(depth) => depth.to_string(),
            Depth::Infinity => "Infinity".to_string(),
        }
    }
}
