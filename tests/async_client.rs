use hyperdav::{AsyncClient, AsyncClientBuilder};
use uuid::Uuid;

const OWNCLOUD_URL: &'static str = "https://demo.owncloud.org/remote.php/webdav/";

fn get_client() -> AsyncClient {
    let uuid = Uuid::new_v4();
    let url = format!("{}{}", OWNCLOUD_URL, uuid);
    AsyncClientBuilder::default()
        .credentials("test", "test")
        .build(&url)
        .unwrap()
}

#[tokio::test]
async fn async_put() {
    let client = get_client();
    let put_res = client.put("", &[""]).await;
    assert!(put_res.is_ok());
}

#[tokio::test]
async fn async_get() {
    let client = get_client();
    let put_res = client.put("", &[""]).await;
    let get_res = client.get(&[""]).await;

    assert!(put_res.is_ok() && get_res.is_ok());
}

#[tokio::test]
async fn async_mkcol() {
    let client = get_client();
    let mkcol_res = client.mkcol(&[""]).await;

    assert!(mkcol_res.is_ok());
}

#[tokio::test]
async fn async_mv() {
    let client = get_client();
    let mkcol_res_root = client.mkcol(&[""]).await;
    let mkcol_res = client.mkcol(&["from"]).await;
    let mv_res = client.mv(&["from"], &["to"]).await;
    assert!(mkcol_res_root.is_ok() && mkcol_res.is_ok() && mv_res.is_ok());
}

#[tokio::test]
async fn async_list() {
    let client = get_client();
    let mkcol_res = client.mkcol(&[""]).await;
    let list_res = client.list(&[""]).await;

    assert!(mkcol_res.is_ok() && list_res.is_ok());
}
